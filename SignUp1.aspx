﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUp1.aspx.cs" Inherits="SignUp1" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">


        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 258px;
        }
        .auto-style3 {
            width: 558px;
        }
        .auto-style4 {
            width: 258px;
            height: 45px;
        }
        .auto-style5 {
            width: 558px;
            height: 45px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    <p>
        <br />
    </p>
    <div>
    

        <p>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CREATE NEW USER PROFİLE&nbsp;</p>
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">E-mail Address:</td>
                <td class="auto-style3">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" BackColor="Red" ControlToValidate="TextBox1" ErrorMessage="Enter yourE- mail adress" Font-Size="Small"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" BackColor="Red" ControlToValidate="TextBox1" ErrorMessage="Please enter correct form" Font-Size="Small" ValidationExpression="^((?:(?:(?:[a-zA-Z0-9][\.\-\+_]?)*)[a-zA-Z0-9])+)\@((?:(?:(?:[a-zA-Z0-9][\.\-_]?){0,62})[a-zA-Z0-9])+)\.([a-zA-Z0-9]{2,6})$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Confirm E-mail Address:</td>
                <td class="auto-style3">
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" BackColor="Red" ControlToCompare="TextBox1" ControlToValidate="TextBox2" ErrorMessage="Please enter the same email address in both fields." Font-Size="Small"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">Password:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBox3" runat="server" TextMode="Password" MaxLength="30"></asp:TextBox>
                    <asp:PasswordStrength ID="TextBox3_PasswordStrength" runat="server" MinimumLowerCaseCharacters="2" MinimumNumericCharacters="2" MinimumSymbolCharacters="2" MinimumUpperCaseCharacters="2" PreferredPasswordLength="8" PrefixText="Strength: 8" TargetControlID="TextBox3">
                    </asp:PasswordStrength>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" BackColor="Red" ControlToValidate="TextBox3" ErrorMessage="Enter a password" Font-Size="Small"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" BackColor="Red" ControlToValidate="TextBox3" ErrorMessage="Password must be between 4 and 8 digits long and include at least one numeric digit." Font-Size="Small" ValidationExpression="^(?=.*\d).{4,8}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Confirm Password:</td>
                <td class="auto-style3">
                    <asp:TextBox ID="TextBox4" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" BackColor="Red" ControlToCompare="TextBox3" ControlToValidate="TextBox4" ErrorMessage="Password mismatch." Font-Size="Small"></asp:CompareValidator>
                </td>
            </tr>
        </table>
        <p>
            <asp:Button ID="Button1" runat="server" style="margin-left: 499px" Text="REGISTER" Width="78px" OnClick="Button1_Click" />
            <asp:Label ID="Label1" runat="server"></asp:Label>
        </p>
    
    </div>
    </form>
</body>
</html>
