﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="capricorn.aspx.cs" Inherits="capricorn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/car.png" /></div>  
        </div>
        <div id="profile"> 

          <p> Capricorns possess a timeless charm. You're the picture of grace—elegant, well mannered, and socially appropriate, like signmate Michelle Obama. Country club memberships fall at your feet, velvet ropes are quickly unclasped in your presence. While you may hobnob with the elite, there's an unwavering practicality about you. A rock-steady earth sign, you lend structure and stability to any project. You'll work tirelessly to ensure that there's food on your family's table and that the people you love enjoy the best the world has to offer. In fact, you show love by providing material comforts, rather than with grand displays of affection. To say you're ambitious is an understatement. When you've fixed your sights on a goal, you'll ascend the steepest mountain trail to reach it. Pure dedication and drive earn you the well-deserved stripes in your industry. Just beware of the pitfalls of prestige. Keeping up appearances can be a job unto itself, and you can get thrown off course trying to please and appease your constituency. 
</p>
            <br />
            <br /> 
         <p>
             In affairs of the heart, you’re attracted to power and status, but you also need a partner who can help keep the home fires burning. Since you're a bit like Wonder Woman and Supergirl rolled into one, anyone less than a heroic figure wouldn't be a suitable mate. You live much of your life in the public eye, and you need an extroverted partner who enjoys socializing as much as you do. Repression is not uncommon among tightly wound Capricorns. You're a "freak in the sheets, lady in the streets" and may develop a sexual alter ego to help you release all that pent-up desire. Physically, your sign is associated with the knees, bones, skin, and teeth. Many Capricorns excel as long-distance runners and bikers; don't shy away from those marathons. Premium dental care and a high-calcium diet are a must if you want to keep up your winning smile and Emily Post–approved posture. 
         </p>
            <br />
            <br />  

            <p>
               <b> Ruler:</b>  Saturn, the planet of maturity, structure, and oracle-like wisdom <br />   <br />  

<b>Your gifts:</b>  drive, ambition, a rock-solid work ethic <br />   <br />  

<b>Your issues:</b>  elitism, overemphasis on external appearances, pessimism <br />   <br />  

<b>Your saving grace:</b>  your ability to make (and keep) friends in high places <br />   <br />  

<b>Your path:</b>  to elevate people's standards by producing high-quality work <br />   <br />  

<b>Your fashion inspiration:</b>  Chloé, Hermès <br />   <br />  

<b>Love 'em:</b>  Taurus, Pisces <br />   <br />  

<b>Notsomuch:</b>  Sagittarius, Libra <br />   <br />  

<b>Celebrity Starmates:</b> Sienna Miller, Kate Moss, Mary J. Blige, Christy Turlington <br />   <br />  
            </p>

           
        </div>

           <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    </div>
    </form>
</body>
</html>
