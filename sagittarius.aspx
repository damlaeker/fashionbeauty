﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="sagittarius.aspx.cs" Inherits="sagittarius" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/star.png" /></div>  
        </div>
        <div id="profile"> 

          <p> Aim high your arrows, Sagittarius. As the Zodiac's Hunter Goddess, you were born on a quest for wisdom and the universal truths. You’re the eternal student, and your mind is endlessly curious, thirsting to know the why and how of any subject that fascinates you. You have a finely honed BS detector and will quickly (and publicly) cry foul if someone's being unscrupulous. Fiercely independent, you thrive as an entrepreneur or a consultant or in a job that allows room for endless creativity. You simply must be passionate about the work you do; anything less would be deadly to your pioneering spirit. Mobility and variety are essential. You're the sign of the traveler, collecting experiences, knowledge, and friends from every part of the globe. People are people as far as you're concerned, and your roster of friends is a rainbow coalition unto itself.
</p>
            <br />
            <br /> 
         <p>

             A fire sign, you heat up quickly when something (or someone) turns you on, but the blazes may die out just as quickly. To say you have commitment issues would be putting it mildly. At the first sign of boredom, you're ready to gallop off to greener pastures. Learning how to stay put during the dull phases of a relationship is one of your greatest lessons. Sagittarius, most people can't be "on" 24/7. Even you require downtime to refuel your tanks—although you're probably lounging around in vintage marabou slippers, teaching yourself conversational French as you plot an upcoming trip to Paris. No, Sagittarius, you'd never be labeled boring, although you can be infuriatingly self-possessed. Be careful not to steamroll other people's ideas as you pursue your own vision quest. If you do, it’s unintentional, and you snap back to reality when a friend delivers a tough-love wakeup call. Your comic timing is also superb. Just as tension is coming to a head, you crack one of your famously off-color jokes or impersonations, leaving everyone in stitches. Ruler of the hips and thighs, you release tension through regular rounds of booty-shaking, nature treks, and lengthy urban hikes. 

         </p>
            <br />
            <br />  

            <p>
               <b> Ruler: </b> Jupiter, the planet of fortune, daring, and studious expansion <br /> <br />

<b>Your gifts:</b> bohemian adventurism, multicultural awareness, well-timed humor <br /> <br />

<b>Your issues:</b> dogmatic preaching, impulsivity, gambling with fate <br /> <br />

<b>Your saving grace:</b> your ability to cut through tension by cracking a joke <br /> <br />

<b>Your path:</b> to unify disparate groups into a "We Are the World" coalition <br /> <br />

<b>Your fashion inspiration:</b> Marc Jacobs, Marni <br /> <br />

<b>Love 'em:</b> Aries, Libra <br /> <br />

<b>Notsomuch:</b> Pisces, Scorpio <br /> <br />

<b>Celebrity starmates:</b> Katie Holmes, Scarlett Johansson, Miley Cyrus, Julianne Moore <br /> <br />
            </p>

           
        </div>

           <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    </div>
    </form>
</body>
</html>
