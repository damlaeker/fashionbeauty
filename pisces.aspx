﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pisces.aspx.cs" Inherits="pisces" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/pis.png" /></div>  
        </div>
        <div id="profile"> 

          <p> You're the zodiac's dreamer, who sees the world through a kaleidoscopic viewfinder. Reality, as far as you're concerned, is highly overrated. You'd rather bring forth the luminous visions of your mind's eye, leaving everyone spellbound in the aftermath. Music, art, dance, sensual touch—you've known since birth that subconscious cues are as strong a language as the spoken word. You're a master of these tools, Pisces, and are capable of wielding a Svengali-like power over the masses. Learn to use your psychic gifts responsibly. The point is to inspire people, not to create cultlike worshippers, and you can tip those scales. No matter your physical appearance, you possess a radiant inner beauty best described as "angelic." Compassionate to the core, you couldn't turn away a stray animal or down-and-out person in need if your life depended on it. As such, you must find ways to guard against exploitation by con artists and the emotional barnacles who love latching onto you. Being the most sensitive of all water signs, you absorb energy like a sponge, often unaware that you're carrying a far too heavy load. When you crash, it’s a mess of passive-aggressive resentment, nervous breakdowns, and chaos.
</p>
            <br />
            <br /> 
         <p>
             In extreme cases, you can veer toward self-destruction like Pisces Kurt Cobain. To remain balanced, surround yourself with guides who can help you release your codependent and destructive urges. The confession booth was practically designed for you, since guilt is a burden you carry. A great therapist, life coach, or spiritual counselor is a must-have. Create a personal retreat center for yourself as well—a one-woman sanctuary where you can unwind in private and shut out the world’s demands. In affairs of the heart, you're a bohemian love child, willing to drop everything and hop on the tour bus with your latest rock-star boyfriend. Unfortunately, you're prone to romantic escapism, losing sight of your own needs in your quest to pump up your partner's self-worth. In a perfect world, you'd strengthen your commitment to your own life path before you walk down the aisle. This helps you choose a mutually supportive and rewarding relationship with a fellow philosopher-poet.
         </p>
            <br />
            <br />  

            <p>
<b>Ruler:</b> Neptune, the planet of enchantment, illusion, and divine guidance <br /> <br />

<b>Your gifts:</b> poetic imagination, compassion, boundless generosity <br /> <br />

<b>Your issues:</b> flightiness, flakiness, delusion, addiction <br /> <br />

<b>Your saving grace:</b> your ability to cast a spell on people with genuine care <br /> <br />

<b>Your path:</b> to walk people down the path to their dreams <br /> <br />

<b>Your fashion inspiration:</b> Alexander McQueen, Betsey Johnson <br /> <br />

<b>Love 'em:</b> Scorpio, Taurus <br /> <br />

<b>Notsomuch:</b> Gemini, Sagittarius <br /> <br />

<b>Celebrity starmates:</b> Drew Barrymore, Emily Blunt, Rihanna, Ellen Page <br /> <br />
            </p>

           
        </div>

           <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    </div>
    </form>
</body>
</html>
