﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tauras.aspx.cs" Inherits="tauras" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/tar.png" /></div>  
        </div>
        <div id="profile"> 

          <p> If Emily Post had a starmate sister, it would surely be you, Taurus. You’re the picture of grace and good manners, born with proper etiquette downloaded into your cranial database. That's not to say you can't get down and dirty. As an earth sign, you can be every bit as rugged as you are fastidious, swearing like a sailor, then chiding a friend for using the wrong fork to eat her salad. Save the occasional sparring session (you have those bull horns for a reason), you prefer to dwell in harmonious surroundings. Your home is the picture of elegance and sensual luxury. You dream of (and probably possess) perfumed votives, Pratesi sheets, La Mer creams, and enough gowns to fill a couture house. Being the practical sort, you likely bought them all on sale or socked away your pennies in a special fund so you could responsibly indulge. The neck, throat, and shoulders are the body parts your sign rules. Many songbirds (and famous orators) are Taureans, including Ella Fitzgerald, Barbra Streisand, Adele, and Lily Allen. Regardless of how well you can keep a tune, you need an outlet for voicing your ideas: a book club, writers group, support circle, or extended remix of the high school debate team.
</p>
            <br />
            <br /> 
         <p>

             Massages are a must-have, since you carry tension in your upper back and shoulders, usually caused by domestic stress. A family loyalist to a fault, you "shoulder" the burden of your relatives' issues, playing the role of the rescuer or rock. Other times, you can veer toward laziness, much like the sleeping bull in the pasture. When you finally do rise from the fainting couch and attack a project, you’re a tireless worker—your steely ethic is recession-proof. Romantically, you’re drawn to status and success and prefer a partner who comes with all the traditional trimmings. Since you like to take periodic "sabbaticals," find a mate who’s capable of being the sole breadwinner, at least from time to time.
         </p>
            <br />
            <br />  

            <p>
<b>Ruler:</b> Venus, the planet of gracious charm, harmony, and love <br /> <br /> 

<b>Your gifts:</b> loyalty, authenticity, solid family values, practical magic <br /> <br /> 

<b>Your issues:</b> stubbornness, moral righteousness, oversimplifying complex matters <br /> <br /> 

<b>Your saving grace:</b> your unwavering ability to prevail through trying circumstances <br /> <br /> 

<b>Your path:</b> to build a solid home base for the people you love <br /> <br /> 

<b>Your fashion inspiration:</b> Burberry Prorsum, Salvatore Ferragamo <br /> <br /> 

<b>Love 'em:</b> Capricorn, Cancer <br /> <br /> 

<b>Notsomuch:</b> Gemini, Aquarius <br /> <br /> 

<b>Celebrity starmates:</b> Megan Fox, Jessica Alba, Uma Thurman, Renée Zellweger <br /> <br /> 
            </p>

           
        </div>
       <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    </div>
    </form>
</body>
</html>
