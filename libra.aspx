﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="libra.aspx.cs" Inherits="libra" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/lib.png" /></div>  
        </div>
        <div id="profile"> 

          <p> You're the zodiac's official Love Goddess, ruled by the amorous callings of Venus. An air sign, you flutter through life with diaphanous fairy wings, but you never get completely lost in the clouds. You have a keen sense of balance; you know intuitively that there can be no yang without yin. When life gets too saccharine, you bring a dose of the savory, harmonizing with a gentle call to "keep it real." As the sign that rules partnerships, commitments, and all things legal, marriage is on the forefront of your mind from an early age. You tend toward idealism and may either marry young or take longer than most to choose a life partner. Once you do, it's Valentine's Day year-round: To Libra, a lush love life is on par with holding a full-time job. 
</p>
            <br />
            <br /> 
         <p>
             You love to spoil and be spoiled in return. Find a generous mate who appreciates the finer things and can bankroll those lavish pleasures you so deeply crave. Money management is a skill you'll need to cultivate. When an objet of beauty and luxury captivates your senses, impulse control goes MIA. Pulling yourself out of financial debt can be one of your greatest challenges and turning points in life. Hopefully you'll learn your lesson before the collection agents come a-knockin'. 
             
         </p>
            <br />
            <br />  

            <p>
                Your refined tastes make you an excellent critic, hostess, and appreciator of the arts. Each sign rules a different body part, and in your case, it's the posterior region. As such, many Libras tend to carry a little surplus "bootylicious" padding. (Your gourmet and sugar addictions don’t help matters.) Work with a nutritionist to balance your diet and a Reiki master to unblock energy in your root chakra. Stock your life with coaches, trainers, and motivational supporters. You love to be comfortable and can veer toward laziness—keep people around to give you a little push.
            </p>

            <br />
            <br /> 

            <p>
<b>Ruler: </b>Venus, the planet of love, elegance, and timeless beauty <br /> <br />

<b>Your gifts:</b> radiant optimism, peacekeeping, advanced social skills <br /> <br />

<b>Your issues:</b> conflict avoidance, obsession over details, superficiality <br /> <br />

<b>Your saving grace:</b> your innate sense of justice <br /> <br />

<b>Your path:</b> to usher forth the spirit of universal love and harmony<br /> 

<b>Your fashion inspiration:</b> Valentino, Chanel<br /> <br />

<b>Love 'em:</b> Pisces, Aquarius<br /><br />

<b>Notsomuch:</b> Taurus, Virgo<br /><br />

<b>Celebrity starmates:</b> Gwen Stefani, Kate Winslet, Gwyneth Paltrow, Naomi Watts<br /><br />
            </p>

           
        </div>

           <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    </div>
    </form>
</body>
</html>
