﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="aires.aspx.cs" Inherits="aires" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>
        <div id="daily"> 


        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/air.png" /></div>  
        </div>
        <div id="profile"> 

          <p> You’re the reigning Queen of Ingenuity, always ready with a never-been-done-before vision or a fresh spin on a time-honored tradition. The media spotlight beams in your direction, and you're a natural once the flashbulbs start popping. As the first sign of the zodiac, being number one is important to you, whether you're competing for a prestigious title or commanding the stage as a solo artist. Scores of dyed-in-the-wool divas are born under the sign of the ram: Aretha Franklin, Celine Dion, Mariah Carey, Diana Ross, Chaka Khan. Your sign rules the head, and your cranium is a treasure trove. At times, you spill over with grand ideas and may suffer from chronic migraines or stress headaches as a result. Because it’s a passionate fire sign, burnout is a constant threat unless you incorporate "decompression" time into your lifestyle. Meditation, spa treatments, and quiet journaling are ideal ways for you to unload. 
</p>
            <br />
            <br /> 
         <p>

             Patience is not your strong suit, so work on strengthening those listening skills. Otherwise, you could miss the juiciest morsel of a conversation or cut people off just as they're about to bare their souls to you. Fiercely independent, you crave attention and alone time in equal measure. Though you love having a bountiful circle of friends, you can't stand when needy people latch onto you. Learn to accept the trappings that go along with being an icon. Your faithful fan base may be an albatross at times, but those minions also shore you up when you lapse into a self-doubting spell. In relationships, you swing between traditionally "masculine" and "feminine" extremes. Since you're such a warrior princess, you need a partner with a fighting spirit—one who would duel his way into your affections. By the same token, you're no shrinking violet. Your true love must also be confident enough to applaud you from the wings while you take center stage. Once you're securely ensconced in an LTR, your desire for independence returns. Many Aries choose to maintain separate residences (or wings of the house) from their partners. For you, a little absence makes the heart grow fonder.
         </p>
            <br />
            <br />  

            <p>
               Ruler: Mars, the planet of drive, ambition, and warrior-goddess spirit

Your gifts: trendsetting, fearless initiative, self-possession

Your issues: self-centeredness, righteous indignation, anger management

Your saving grace: always being 10 steps ahead of the curve

Your path: to bring to life a truly original concept that influences an industry at large

Your fashion inspiration: Galliano, Miu Miu <br /> <br />

      Your path: to create the spirit of family wherever you go <br /> <br />
 
     Your fashion inspiration: Louis Vuitton, Badgley Mischka <br /> <br />

       Love 'em: Scorpio, Capricorn <br /> <br />

       Notsomuch: Sagittarius, Aquarius <br /> <br />

      Celebrity starmates: Jessica Simpson, Lindsay Lohan, Gisele Bündchen, Pamela Anderson <br /> <br />
            </p>

           
        </div>

           <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    
    </div>
    </form>
</body>
</html>
