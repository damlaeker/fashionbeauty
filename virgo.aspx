﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="virgo.aspx.cs" Inherits="virgo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/vir.png" /></div>  
        </div>
        <div id="profile"> 

          <p> Although you’re the noted sign of the virgin, this epithet can be widely interpreted. What's common among Virgos: insistence on absolute purity in one area of life. You may be a complete minx in the bedroom but a devout raw-foodist who won't eat a morsel that's been heated above 46 degrees. Your garage may look like storage space for Antiques Roadshow, while your gleaming kitchen floors are pristine enough to serve dinner on the parquet. Bottom line: Your hungry mind must have something to obsess about, some subject to study with the fascination of Einstein exploring relativity. You’re the accidental scholar and the eternal student of life, gaining expertise and mastery as a matter of course. Ruled by communicative Mercury, you need an outlet for espousing your ideas, practical wisdom, and political perspectives—social media and the blogosphere were practically invented for you. As an earth sign, you thrive in a structured environment and need to put regular routines in place in order to ground yourself. Your calendar should be treated like a Bible—otherwise, you become stressed-out and scattered. Plan everything, from date nights to fitness to the time you'll break for lunch each day. 
</p>
            <br />
            <br /> 
         <p>
             Since you rule the stomach and abdominal region, digestion-friendly meals and core-based exercises should be incorporated into your master plan. A Pilates "Reformer" would be a worthwhile investment. You're family-oriented by nature and should seek out a partner who is equally devoted to his clan. Having a peaceful home base is essential to your bliss. Decorate with hues found in nature and keep everything eco-friendly, from your allergen-free mattress to your cleaning products. Sustainable architecture and design can be a Virgo fascination. Your maternal instincts are strong: You're a wise and patient parent to children and pets alike. Having someone to love helps funnel your neurotic streak in a productive direction. Just beware of a tendency to smother loved ones with too much nurturing or to get trapped in a codependent dynamic. 
             
         </p>
            <br />
            <br />  

            <p>
             <b>  Ruler: trivia-hungry, quick-witted, erudite Mercury</b> <br /> <br />

<b>Your gifts:</b> modesty, a talent for managing details, a desire to serve humankind <br /> <br />

<b>Your issues:</b> perfectionism, judgmental righteousness, icy control <br /> <br />

<b>Your saving grace:</b> your vast storehouse of practical wisdom and homespun remedies <br /> <br />

<b>Your path:</b> to bring resources to an underserved population <br /> <br />

<b>Your fashion inspiration:</b> Stella McCartney, Proenza Schouler <br /> <br />

<b>Love 'em: Cancer, Gemini </b> <br /> <br />

<b> Notsomuch: </b> Aquarius, Leo <br /> <br />

<b>Celebrity starmates:</b> Beyoncé, Jennifer Hudson, Blake Lively, Amy Winehouse  <br /> <br />
            </p>

           
        </div>

           <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    </div>
    </form>
</body>
</html>
