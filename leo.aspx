﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="leo.aspx.cs" Inherits="leo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/le.png" /></div>  
        </div>
        <div id="profile"> 

          <p> Like a lioness stalking the jungle, you're ever on the hunt for that which feeds your soul (and the souls of your entire pride). You're the zodiac's Queen of Fierce, a born leader and trendsetter who raises the bar of success to breathtaking heights. The simple life is abhorrent—you're a fire sign who deals in bright, colorful blazes. While you're always aflame with creative vision, you’re oft unaware of your might. At times, you "smoke out" potential collaborators by coming on too strong or burn yourself out by juggling too many projects. Your inner superstar must be managed if she is to reach her destined heights. In the not-too-distant past, Leo Jennifer Lopez figured out the code. Within a five-year span, she dropped platinum albums, starred in a series of box-office-friendly rom-coms, launched a successful clothing line, and scored proposals from two Hollywood leading men. How did she pull it off? She learned the essentials of royalty: Gather a five-star court to do your queenly bidding. 
</p>
            <br />
            <br /> 
         <p>
             Face it, Leo, you can be a sucker for flattery. Your best friends, collaborators, and romantic partners aren't the ones who will blow sunshine up your arse or add sparkle to your image when you strut down the red carpet. You need scathingly honest, loyal supporters who aren't afraid to call you out when you've strayed from your original intentions. In affairs of the heart, you're decidedly starry-eyed and must take extra precautions not to entangle yourself in the wrong relationship. Evidenced by signmates Whitney Houston, Madonna, and Halle Berry, you don't always make the best decisions in your first, second, or even third husbands. Battle against your own projections and fantasies; get to know a guy before you start picking out china patterns. Otherwise, you'll wind up carrying the weight of the relationship instead of experiencing the joy of an equally invested partner. Most important, Leo, never let yourself get bored. That's when the shadow side of your personality emerges…the one that thrives on drama. You're capable of starting a tabloidworthy storm when things get ugly. Fame is your destiny, so dive into your passions instead of falling into the notoriety trap.

            
         </p>
            <br />
            <br />  

            <p>
<b>Ruler:</b> that magnificent, larger-than-life, passionate fireball the sun <br />  <br /> 

<b>Your gifts:</b> fearless leadership, boundary-defying creativity, emotional expression <br />  <br /> 

<b>Your issues:</b> overcommitting, impracticality, egomania <br />  <br /> 

<b>Your saving grace:</b> the ability to turn any event into a Cirque du Soleil production <br />  <br /> 

<b>Your path:</b> to call forth the muse with a thundering, passionate invitation <br />  <br /> 

<b>Your fashion inspiration:</b> Roberto Cavalli, Dolce & Gabbana <br />  <br /> 

<b>Love 'em:</b> Aries, Scorpio <br />  <br /> 

<b>Notsomuch:</b> Aquarius, Virgo <br />  <br /> 

<b>Celebrity starmates:</b> Madonna, Jennifer Lopez, Charlize Theron, Halle Berry <br />  <br /> 
            </p>

           
        </div>
           <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    
    </div>
    </form>
</body>
</html>
