﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="gemini.aspx.cs" Inherits="gemini" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/gem.png" /></div>  
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </div>
        <div id="profile"> 


          <p> Who says there's a limit to how many influences one person can have? You're the consummate collector of cool, endlessly curious and always aware of the latest and greatest offerings out there. As an air sign, you tend to breeze between interests, chasing every flight of fancy that grabs your attention. This is where your "split personality" reputation originates: One minute, you're totally obsessed with 18th-century Russian poetry, the next, you're on to postmodern feminist literature. The Internet was tailor-made for your needs, giving you the ability to research a topic at lightning speed and chat with virtual strangers (many of whom become your loyal companions and confidantes). While you hop around gathering data and friends alike, you must guard against your own tendency toward superficiality. Still yourself through fits of boredom, Gemini. Your most electrifying growth will come as a result of your self-imposed ennui. Since your mind is quick and curious, you should always be enrolled in a workshop or short-term course of study, like an art or music class. 
</p>
            <br /> 
         <p>
             Your sign rules the hands and arms, giving you above-par manual dexterity. No surprise many tennis players are Geminis, including Anna Kournikova and Venus Williams. People are your number one fascination. You love to study them, pick apart their behavior patterns, write songs about them, and (unfortunately) gossip like Perez Hilton's evil twin. Find a healthy outlet for your social research, like filming a documentary about an immigrant population in your city or starting a memoir-writing club to develop your life story into an inspiring work of literary mastery. The right romantic partner is endlessly playful, exploratory, and consistent (someone's got to hold down the fort). He must also have supreme listening skills, since you process everything through conversation.
         </p>
            <br />
            <br />  

            <p>
<b> Ruler:</b> clever, communicative, nomadic Mercury <br /> <br />

<b>Your gifts:</b> quick-wittedness, dexterity, spontaneity, a rock star's edge <br /> <br />

<b>Your issues:</b> rash impulsivity, verbal diarrhea, gossiping <br /> <br />

<b>Your saving grace:</b> your ability to reinvent yourself at warp speed <br /> <br />

<b>Your path:</b> to translate complex ideas into a popular-culture dialogue <br /> <br />

<b>Your fashion inspiration:</b> Zac Posen, Dior <br /> <br />

<b>Love 'em:</b> Sagittarius, Leo <br /> <br />

<b>Notsomuch:</b> Taurus, Pisces <br /> <br />

<b>Celebrity starmates:</b> Angelina Jolie, Naomi Campbell, Nicole Kidman, Natalie Portman  <br /> <br />
            </p>

           
        </div>

           <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    </div>
    </form>
</body>
</html>
