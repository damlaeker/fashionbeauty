﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="scorpio.aspx.cs" Inherits="scorpio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/scor.png" /></div>  
        </div>
        <div id="profile"> 

          <p> Mystical, powerful, and otherworldly, you operate on a plane of existence to which few have access. In many ways, Scorpio, you’re an enigma unto yourself, scarcely understanding your own magical abilities. Accept the fact that you funnel knowledge from a source so deeply intuitive, it can't be explained in earthly terms. The real question is, Will you use your powers for good or evil? As a water sign, you’re primarily an emotional being. When your feelings are wounded, your instinct is to strike back with double force, delivering the poisonous sting of the scorpion. You can do damage (and how!), but you're also capable of blessing people with the guidance of an "earth angel." Legend has it that Scorpios evolve through three phases of consciousness: from scorpion to serpent to high-flying eagle. When you cultivate the ability to soar above your reactions, your true powers are unleashed. Holding people's hands through turmoil, walking them through hell until they reach Nirvana, unlocking their pent-up life-force energy and libidos—you do it without a single crack in your emotional veneer. As such, people regularly come to you with their deepest confessions and crises. You respect confidentiality with the loyalty of a CIA agent and will never blab a friend's biz to the rumor mill. (Lord knows you've got your own storehouse of secrets!) 
</p>
            <br />
            <br /> 
         <p>

             Career-wise, you thrive in a cutting-edge work environment where you’re allowed to invent new rules and ascend to unlimited heights of power, just as Bill Gates did with technology, and Diddy with hip-hop (both Scorpios). As the zodiac's famed sex symbol, physical attraction is a top-tier requirement. As frisky as you can be, you're also capable of long bouts of chastity. Sex is spiritual and all-encompassing, and you’ve likely been burned by giving your affections to an unattainable lover. The subsequent obsessions may have lasted years (and annoyed the hell out of your friends and family). Unfortunately, you just couldn't help yourself. You're wired to bond fully, intimately, and addictively with a mate. For that reason, you'd be wise to pick a partner who loves you enough to inject you into his veins.
         </p>
            <br />
            <br />  

            <p>
<b> Ruler: </b> Pluto, the master of mystical transformations and hidden powers <br /> <br /> 

<b>Your gifts:</b> spot-on intuition, fierce loyalty, resourcefulness, creative genius <br /> <br /> 

<b>Your issues: suspicion, guardedness, addiction, obsessing <br /> <br /> 

<b>Your saving grace:</b> your keen ability to read people's needs and pre-empt them <br /> <br /> 

<b>Your path:</b> to guide people through their fears, secrets, and taboos in order to cause a transformation <br /> <br /> 

<b>Your fashion inspiration:</b> Gucci, Balenciaga <br /> <br /> 

<b>Love 'em:</b> Taurus, Capricorn <br /> <br /> 

<b>Notsomuch:</b> Aquarius, Aries <br /> <br /> 
 
<b>Celebrity starmates:</b> Anne Hathaway, Karen O, Demi Moore, Björk <br /> <br /> 
                </p>
                

           
        </div>
       <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    </div>
    </form>
</body>
</html>
