﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Fashion1
/// </summary>
public class Fashion1
{
	public Fashion1()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    string ConStr = @"Server=DAMLA;Database=FashionBeautyDB;Trusted_Connection=True;";

    public DataSet GetAllUsers()
    {
        DataSet ds = new DataSet();

        SqlConnection myConn = new SqlConnection(ConStr);

        myConn.Open();

        SqlCommand myCmd = new SqlCommand("SELECT [Email],[password] FROM Users", myConn);

        SqlDataAdapter myAdapter = new SqlDataAdapter(myCmd);

        myAdapter.Fill(ds);

        myConn.Close();

        return ds;
    }

    public void DeleteUser(int userID)
    {

        SqlConnection myConn = new SqlConnection(ConStr);

        myConn.Open();

        SqlCommand myCmd = new SqlCommand("DELETE FROM [dbo].[Users] WHERE userID = " + userID, myConn);

        myCmd.ExecuteNonQuery();

        myConn.Close();
    }

    public void AddUsers(string Email, string password)
    {
        SqlConnection myConn = new SqlConnection(ConStr);

        myConn.Open();

        SqlCommand myCmd = new SqlCommand("INSERT INTO [dbo].[Users] " +
           "([Email], [password]) VALUES " +
           "('" + Email + "', '" + password + "' )", myConn);

        myCmd.ExecuteNonQuery();

        myConn.Close();

    }

   

    public int UpdateUser(string Email, string password, int userID)
    {
        SqlConnection myConn = new SqlConnection(ConStr);

        myConn.Open();

        SqlCommand myCmd = new SqlCommand("UPDATE [dbo].[Users] " +
   "SET [Email] = @Email1, [password] = @password1" +
   " WHERE [userID] = @userID", myConn);

        myCmd.Parameters.Add("@Email1", SqlDbType.NVarChar).Value = Email;
        myCmd.Parameters.Add("@password1", SqlDbType.NVarChar).Value = password;
        
        myCmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;

        int Result = Convert.ToInt32(myCmd.ExecuteNonQuery());

        myConn.Close();

        return Result;

    }

    public DataSet GetUsersByID(int userID)
    {
        DataSet ds = new DataSet();

        SqlConnection myConn = new SqlConnection(ConStr);

        myConn.Open();

        SqlCommand myCmd = new SqlCommand("SELECT [Email],[password] FROM Users WHERE userID = @userID", myConn);

        myCmd.Parameters.Add("@userID", SqlDbType.Int).Value = userID;

        SqlDataAdapter myAdapter = new SqlDataAdapter(myCmd);

        myAdapter.Fill(ds);

        myConn.Close();

        return ds;
    }

   
    public int UpdateDaily(string aires, string taurus, string gemini, string cancer,
        string leo, string virgo, string libra, string scorpio, string sagittarius,
        string capricorn, string aquarius, string pisces, int dayID)
    {
        SqlConnection myConn = new SqlConnection(ConStr);

        myConn.Open();

        SqlCommand myCmd = new SqlCommand("UPDATE [dbo].[Daily] " +
   "SET [aires] = @aires1, [taurus] = @taurus1 ,[gemini] = @gemini1"+
   ",[cancer] = @cancer1,[leo] = @leo1,[virgo] = @virgo1,[libra] = @libra1,"
   +"[scorpio] = @scorpio1,[sagittarius] = @sagittarius1,[capricorn] = @capricorn1,[aquarius] = @aquarius1,[pisces] = @pisces1"
   +"WHERE [dayID]=@dayID", myConn);

        myCmd.Parameters.Add("@aires1", SqlDbType.NVarChar).Value = aires;
        myCmd.Parameters.Add("@taurus1", SqlDbType.NVarChar).Value = taurus;
        myCmd.Parameters.Add("@gemini1", SqlDbType.NVarChar).Value = gemini;
        myCmd.Parameters.Add("@virgo1", SqlDbType.NVarChar).Value = virgo;
        myCmd.Parameters.Add("@leo1", SqlDbType.NVarChar).Value = leo;
        myCmd.Parameters.Add("@libra1", SqlDbType.NVarChar).Value = libra;
        myCmd.Parameters.Add("@aquarius1", SqlDbType.NVarChar).Value = aquarius;
        myCmd.Parameters.Add("@capricorn1", SqlDbType.NVarChar).Value =capricorn;
        myCmd.Parameters.Add("@pisces", SqlDbType.NVarChar).Value = pisces;
        myCmd.Parameters.Add("@scorpio1", SqlDbType.NVarChar).Value = scorpio;
        myCmd.Parameters.Add("@cancer1", SqlDbType.NVarChar).Value = cancer;
        myCmd.Parameters.Add("@sagittarius1", SqlDbType.NVarChar).Value = sagittarius;
        myCmd.Parameters.Add("@dayID", SqlDbType.Int).Value = dayID;

        int Result = Convert.ToInt32(myCmd.ExecuteNonQuery());

        myConn.Close();

        return Result;

    }
    public DataSet GetAires()
    {
        DataSet ds = new DataSet();

        SqlConnection myConn = new SqlConnection(ConStr);

        myConn.Open();

        SqlCommand myCmd = new SqlCommand("SELECT [aires] FROM daily", myConn);

        SqlDataAdapter myAdapter = new SqlDataAdapter(myCmd);

        myAdapter.Fill(ds);

        myConn.Close();

        return ds;
    }

}

