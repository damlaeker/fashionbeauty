﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="aquaris.aspx.cs" Inherits="aquaris" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="style.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="header">


                <a href="LogIn.aspx" id="l"> LOG IN </a>
              <a href="SignUp.aspx" id="s"> SIGN UP</a>



                   
               <a href="#"> <img src="IMAGES/flagEn.png" />  </a> 
              
        
    </div>

    <div id="menu">

        <div id="logo">
         <a href="#" id="i" ><img src="IMAGES/logoson.png" /></a> 
        </div>
        
        <div id="writing">

        <ul>
        <li> <a href="#" id="m"> FASHION </a>
            <ul>
                <li> <a href="#">NEWS</a>  </li>
                 <li> <a href="#">TODAY I'M WEARING</a>  </li>
                 <li> <a href="#">O MU BU MU</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="g"> BEAUTY </a>
            <ul>
                <li> <a href="#">BEAUTY TRENDS</a>  </li>
                <li> <a href="#"> </a>  </li>
                <li> <a href="#">ESTETİK</a>  </li>
                <li> <a href="#"></a>  </li>
                <li> <a href="#">SAÇ</a>  </li>



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="ü"> ACCESSORIES </a>
            <ul>
                <li> <a href="#">TRENDS</a>  </li>
                <li> <a href="#">SHOP ACCESORIES</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="d"> HOROSCOPES  </a>
            <ul>
                <li> <a href="#">ABOUT</a>  </li>
                <li> <a href="#"> PROFILE</a>  </li>
                



            </ul>

            </li>
            </ul>

             <ul>
        <li> <a href="#" id="e"> GALLERY </a>
            <ul>
                <li> <a href="#">PHOTO GALLERY</a>  </li>
                <li> <a href="#">VİDEO GALLERY</a>  </li>
                <li> <a href="#">BEST OF</a>  </li>



            </ul>

            </li>
            </ul>

         
            
        </div>
        </div>

        <div id="logohoros">

          <div id="logoelement"><img src="IMAGES/aqu.png" /></div>  
        </div>
        <div id="profile"> 

          <p> Fly that freak flag high, Aquarius. You may look like an all-American girl on the surface (or not), but behind that distressed-denim jacket beats the heart of a true bohemian. You have the unique ability to combine logic and creativity into pure genius. Few people understand your vision when you simply talk about it. Get into action. Once a tangible product has been brought forth, you catapult to the heights of popularity and respect, like barrier-smashing talk-show queens Oprah Winfrey and Ellen DeGeneres. You have an innate concern for human rights—especially when it comes to fighting for the underdog. Find an appropriate outlet for your inner revolutionary, the more public the better. As an air sign, you’re a gifted communicator who is capable of reaching the masses with your message. Like Aquarius author Alice Walker and comedian Chris Rock, your world vision can be funneled through moving works of fiction or politically charged comedy. Few people can match you when it comes to humor, which also makes you popular with a broad spectrum of people. As the sign that rules groups, you thrive in a collaborative environment, managing your teammates with the motivation of a cheerleading captain.
</p>
            <br />
            <br /> 
         <p>
             Romantically, you could use a little more oomph. Since you generally eschew all things sappy, you may deprive yourself of the lovey-dovey emotions that go along with relationships. A partner who is in tune with the emotional realm would be ideal for you, even if he feels a bit like an alien from time to time. Work on cultivating a clear connection to your own wild streak. Otherwise your connections could fall flat; or your feelings may explode in sudden bursts of anger. You're more comfortable raging than you are crying—you'll need a release for those pent-up emotional lightning strikes. Your sign rules the calves and ankles, so activities like running and biking could become marathon-level obsessions for you. Team sports are also a happy fit for your sign's needs. Scout out the local kickball league or roller derby club and pile on the bonus cool points—not like you really need them. 
         </p>
            <br />
            <br />  

            <p>
               <b> Ruler:</b> Uranus, the planet of innovation, revolution, and surprises <br /> <br />

 <b>Your gifts:</b> ingenuity, open-mindedness, a desire to fight for the underdog <br /> <br />

 <b>Your issues:</b> people pleasing, coldness, flashes of unexpected rage <br /> <br />

 <b>Your saving grace:</b> an endless roster of friends owing you favors <br /> <br />

 <b>Your path:</b> to create a society based on true equality and democracy <br /> <br />

 <b>Your fashion inspiration:</b> Anna Sui, Céline <br /> <br />

 <b>Love 'em:</b> Gemini, Libra <br /> <br />

 <b>Notsomuch:</b> Virgo, Cancer <br /> <br />

 <b>Celebrity starmates:</b> Jennifer Aniston, Alicia Keys, Ellen DeGeneres, Oprah Winfrey <br /> <br />
            </p>

           
        </div>

           <div id="social">

                 <a href="#" ><img src="IMAGES/face.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"><img src="IMAGES/twitter.png" style="height: 30px; width: 30px" /> </a>
                 <a href="#"> <img src="IMAGES/insta.png" style="height: 30px; width: 30px" /></a>
                 <a href="#"> <img src="IMAGES/google-plus-128.png" style="height: 30px; width: 30px" /></a>

            </div>
    </div>
    </form>
</body>
</html>
